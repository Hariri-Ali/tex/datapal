\section{Introduction}
Over the past years, the protection of user privacy and the proper handling of user data has become a major concern with increasing importance \cite{gdpr}.
This is caused by the rise of ubiquitous internet (e.g., smart devices, social networks, etc.) that generate, collect and share huge amounts of personal data such as location information, video feeds and user behaviour \cite{iotdata}.
Furthermore, these concerns were fueled by many privacy-related incidents in which personal data was abused and exploited, such as the most famous scandal the Cambridge Analytica \cite{davies2015ted}.

As a response to the need for protecting citizens' privacy and ensure the appropriate use of their data, laws such as the \ac{gdpr}, have been introduced \cite{gdpr}. In these days, many initiatives like the GAIA-X project\footnote{https://www.gaia-x.eu/} have been launched in order to enforce policies to control data access and usage, as well as to guarantee that smart systems are privacy-preserving and compliant with regulations.

One of the privacy protection principles that must be supported in such systems is known as intervenability, which offers the ability for users to intervene (e.g. withdraw their data) in case inconsistencies occur with their data.
In addition, it must be guaranteed that the users are informed about how their data will be used and their consent allowing or denying the processing of their data must be obtained. Also, it must be possible for users to withdraw their consent even after data processing has already started.

In our previous work \cite{DBLP:journals/jisis/BaldiDDMMMOS20}, we presented a session-dependent authorization framework that provides an enhanced, expressive and flexible data protection support in big data environments.
The framework organises authorizations in a hierarchy that represents dependencies between them.
As such, if an authorization was revoked due to an attribute update, all subsequent and dependent authorizations would be revoked.
This allows the system to end any data processing operations if the corresponding authorization conditions do not hold anymore.

In another work, we introduced \ac{ucsp} \cite{DBLP:conf/trustcom/DimitrakosDKMMR20}, a novel trust-aware continuous authorization framework for \ac{iot}.
The framework is a fusion of \ac{abac} \cite{abac} policy engine and a \ac{tlee}.
It uses a policy language known as the \ac{alfa} \cite{alfa-oasis} and extends it with trust-level evaluation formulae, and supports contextualised and continuous monitoring of attributes as well as re-evaluation of policies.

In this paper, we extend \ac{ucsp} and \ac{alfa} to support \ac{adp} in \ac{alfa} policy language by incorporating administrative policies
which are particular policies to define administration and delegation.
More importantly, we introduce a new policy model that uses administrative policies to support user consent and intervenability, namely the \ac{adp}.
The model allows data owners to define data-centric policies to be enforced in each and every step of the lifecycle of their data (i.e. collection, retention, treatment and deletion), giving them full control over their data and privacy.
An example of data lifecycle is represented in Figure \ref{fig:datalifecycle} where the various stages of collection, access, usage, storage, transfer and deletion are highlighted. 

\begin{figure}[!htb]
    \centering
    \includegraphics[width=\linewidth]{images/datalifecycle.png}
   \caption{Data Lifecycle \cite{danwilliams}}
    \label{fig:datalifecycle}
    \centering
\end{figure}

It also supports separation of concerns and shared responsibility.
In addition, the model helps in compliance by using root of trust administrative policies to encode regulations that define what data owners and data processors can do about the data.
Thanks to \ac{ucsp}, administrative policies can be re-evaluated when a change occurs and authorization decisions can be updated accordingly.

The rest of the paper is organized as follows: 
Section \ref{sec:background} presents the related work.
In Section \ref{sec:architecture} We outline the architecture of UCS+ and in Section \ref{sec:policymodel}, we introduce our policy model. We describe the policy language used, the administration and delegation profile, the policy editing and evaluation workflows, the data lifecycle policies and then provide an example use-case.
Section \ref{sec:performance} refers to our prototype implementation and discusses the results of the performance analysis in realistic cloud settings.
Finally, we conclude in Section \ref{sec:conclusion} by summarizing the main innovations and discussing future research directions.
