\section{Architecture}\label{sec:architecture}
Our system \ac{ucsp} supports continuous context monitoring, usage revocation and policy re-evaluation, and enables secure data sharing and control.
This makes it a perfect fit to address the objectives highlighted in the Introduction section.
Namely, intervenability and consent, where users are able to define, via policy, how their data can be collected, used or transferred and eventually how each action should be performed.
\ac{ucsp} is implemented according to a message-centric integration architecture inspired by microservices.
It leverages in-line context enrichment and the \ac{pubsub} pattern in order to maximise concurrency between policy parsing, policy evaluation and attribute retrieval.
As such, \ac{ucsp} enhances performance and reduces the need for low network latencies or high computational resources.
It also improves the  ability to upgrade or substitute components and services and to migrate to a distributed deployment where necessary.

\ac{ucsp} incorporates the core components of a standard \ac{ucon} framework, as well as new components that support the additional functionalities and optimisations of \ac{ucsp}.
The architectural components are shown in Figure \ref{fig:bloc-architecture} and briefly described as follows: 
\begin{itemize}
\item \texttt{\ac{ch}} is the core component of the framework, acting as message enricher and manager of the operative workflow. 
\item \texttt{\ac{msgbus}} is a sub-component of the \ac{ch} that implements and supports the \ac{pubsub} communication paradigm.
\item \texttt{\ac{sm}} is a vital component enabling the continuity of control as well as the storage of information about all active sessions.
\item \texttt{\ac{om}} handles and manages policy and rule obligations. 
\item \texttt{\ac{at}} is an auxiliary component that manages and caches attributes. It is needed in faulty environments where attribute values are not available right away when queried.
\item \texttt{\ac{ar}} is an auxiliary component in charge of querying and updating attribute values.
\item \texttt{\ac{pip}} defines where to find attributes and how to monitor them and collect their values.
\item \texttt{\ac{pip} Registry} manages \acp{pip} and defines which \acp{pip} are responsible for which attributes.
\item \texttt{\ac{pep}} receives evaluation requests (PEPA), interacts with the \ac{ch} to evaluate them, and returns an evaluation result (PEPO).
\item \texttt{\ac{pdp}} is the component that evaluates policies matching a specific request, and returns one of the following possible decisions:
\begin{itemize}
    \item \texttt{PERMIT};
    \item \texttt{DENY};
    \item \texttt{NOT APPLICABLE:} Decision cannot be taken due to semantic reasons, i.e. no rules in the policy are matched; or
    \item \texttt{INDETERMINATE:} Decision cannot be taken since either the policy or the request are malformed.
\end{itemize}
The \ac{pdp} leverages \ac{abacee} to perform \ac{abac} policy evaluation, and supports auxiliary evaluators. 
As such a dedicated evaluator that supports \ac{adp} has been implemented and plugged inside \ac{pdp} with minimal effort.
\item \texttt{\ac{pap}} is a management interface used to create, read, update and delete policies. It stores and manages policies and is used by the \ac{pdp} to retrieve applicable policies.
\end{itemize}

\begin{figure}[!htb]
    \centering
    \includegraphics[width=1\linewidth]{images/ucs-architecture2.png}
    \caption{Architecture with interconnections}
    \label{fig:bloc-architecture}
    \centering
\end{figure}
