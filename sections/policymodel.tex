\section{Policy Model}\label{sec:policymodel}
\subsection{\acf{adp}}
\ac{adp} \cite{oasis2014admindel} is a profile introduced in XACML 3.0 to address use-cases in which administration of policy issuance or delegation of policy authority is required.
For instance, system administrators or resource owners may want to restrict and control who can write policies about their resources.
As another example, a resource owner may want to delegate their authority to someone else while they are away or on vacation.
To support such use-cases, the \ac{adp} allows administrators to write policies about other policies forming trees that start with top level policies designated as root of trust. 
In the scope of this paper, we refer to controller policies (i.e. policies that control other policies) as \textit{Administrative Policies}.
On the other hand, we designate controlled policies (i.e. access policies) as \textit{Usage Policies}.
As such, administrative policies define who is authorized to write other administrative policies or specific usage on data, resources or services.

According to the \ac{adp}, a Usage Policy will not be enforced unless it is explicitly authorized by Administrative Policies.
Thus, a Usage Policy can be \textit{Admissible} if its enforcement is authorized by all upper layer policies, or \textit{NotAdmissible} if one of the Administrative Policies in the hierarchy cannot be considered.
This is expressed in the evaluation outcomes truth table illustrated in Table \ref{tab:evaluation outcomes}, 
which shows that whenever Administrative Policies are admissible, a final result is obtained.
On the other hand, if one of the Administrative Policies is NotAdmissible, then another policy needs to be found.
Indeed, in some cases, multiple policies may be Admissible and have a Permit/Deny result.
For this reason, a so-called policy reduction algorithm is used.
It is of vital importance to manage inconsistencies and to guide editing of administrative policies because otherwise all evaluations will end up in a NotAdmissible state. 
To achieve this, once an administrative policy is added, compliance with the above layers can be checked and suggestions for improvements may be provided.
Therefore editing a full-fledged policy with \ac{adp} resembles the one shown in Listing \ref{lst:policyediting}.

\begin{table}[!htbp]
    \centering
    \caption {Evaluation Outcomes} \label{tab:evaluation outcomes}
    \resizebox{0.99\linewidth}{!}{
    \begin{tabular}{|c|c|c|}
            \hline
            \textbf{Usage Policy}  & \textbf{Administrative Policy} & \textbf{Result} \\
            \hline
            Permit & Admissible & Permit \\
            \hline
            Deny & Admissible & Deny \\
            \hline
            Permit & NotAdmissible & Find other policy \\
            \hline
            Deny & NotAdmissible & Find other policy \\
            \hline
        \end{tabular}
    }
\end{table}

\begin{lstlisting}[float=!htb, caption={Evaluation and Reduction process},label={lst:policyediting}, escapeinside={(*}{*)}]
<given a root policy defined by authority / organization>
user writes custom policy Pa
synthetic request is crafted from Pa
request is evaluated against Pa
    select administrative policy Padmin for Pa
        for each Pi in Padmin
            create administrative request Ra using R and Pi
            evaluate Ra against Pi
            if deny
                <show modifications>
            else
                <allow policy>
\end{lstlisting}


\subsection{Extended Policy Language}
\ac{ucsp} uses \ac{alfa} \cite{alfa-oasis} as a baseline policy language.
\ac{alfa} is a pseudocode domain-specific policy language that maps directly to \ac{xacml} \cite{xacml} without adding any new semantics.
It is much less verbose than \ac{xacml}, which makes it more human readable and shorter in size allowing faster parsing and evaluation. 
\ac{alfa} adheres to the same hierarchy as \ac{xacml} where decision predicates are expressed in rules that are nested under policies, which are in turn nested under policy sets.
\ac{alfa} policy sets may also enclose other policy sets, and rules must evaluate into either \emph{permit} or \emph{deny}. 
Like \ac{xacml}, \ac{alfa} relies on combining algorithms to resolve conflicts between sibling rules or policies.
\ac{alfa} also allows the use of functions, such as regular expression, string concatenation and others, which helps to further refine applicability of policies and rules.
Accordingly, an \ac{alfa}  policy must adhere to the non-inclusive structure shown in Listing \ref{lst:policy_structure}.

In our previous work \cite{DBLP:conf/trustcom/DimitrakosDKMMR20}, we extended \ac{alfa} by classifying policy rules as ``\textit{pre}'', ``\textit{ongoing}'' and ``\textit{post}'', adding an implicit temporal state to authorizations.
In this work, we introduce another extension to \ac{alfa} to support \ac{adp}.
We specifically introduced the ``\texttt{PolicyIssuer}'' keyword, which identifies the author of the policy, and is used to create an administrative request.
In addition, we reserved the following attribute categories to be used for \ac{adp}:
\begin{itemize}
    \item \texttt{Attributes.delegate} attribute category used in administrative requests to carry the attributes of the issuer of the policy which is being reduced.
    \item \texttt{Attributes.delegation-info} attribute category used in administrative requests to carry information about the reduction in progress, such as the decision being reduced.
    \item \texttt{Attributes.delegated} category used to carry information about the situation which is being reduced.
\end{itemize}
These extensions support the policy model and policy editing workflow described in the following subsection.

\begin{lstlisting}[float=!htb, caption={Structure of an \ac{alfa} policy},label={lst:policy_structure}]
policy <Title> {
  target clause
            Attributes.<AttributeName> == <AttributeValue>
  apply <CombiningAlgorithm>
  rule <RuleName> {
    target clause
            Attributes.<AttributeName> == <AttributeValue>
    condition <ConditionExpression>
    <decision (permit or deny)>
    on <decision> {
      <instruction (obligation)> <ObligationName> { 
        <Attribute> = <Value>
}}}}
\end{lstlisting}

\subsection{Policy Editing and Evaluation Workflow}\label{sub:polediting}
Using the extensions we added to \ac{alfa}, we introduce the following policy editing workflow, which is also demonstrated in the example workflow shown in Figure \ref{fig:policyediting}.
The \textit{\ac{ciso}} is in charge of creating top-level root-of-trust administrative policies that scope organisational, regulatory and business constraints.
For instance, the \ac{ciso} specifies the actions, resources, usage purposes and other attributes that \ac{ds} and other actors are allowed to control in their policies.
%Managers can also add another level of administration by adding administrative policies that scope departmental regulations and constraints.
\textit{\ac{ds}} (i.e. data owner) is delegated by the \ac{ciso}, and is in charge of creating administrative policies that delegate authority to other actors (e.g. managers).
\ac{ds} policies define who can write usage policies about their data, and what kind of actions and data processing operations can these usage policies control.
Finally, managers write usage policies about the data and define the constraints for data usage and processing. 
In summary, the \ac{ciso} group delegates to the \ac{ds} group, and the \ac{ds} group in turn delegates to the managers group, which permits or denies staff from accessing data.
An example of an administrative and usage policies is shown in Listing \ref{lst:admin_policy}, where the \ac{ciso} policy allows all data subjects to write policies about any action performed on their data.
Similarly, the data subject policy allows any manager to write policies about CRUD operations performed on the data. 
Finally, the staff policy is a usage policy that allows staff member Bob to read the data.


\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.45]{images/DataSubject.png}
    \caption{Policy editing workflow}
    \label{fig:policyediting}
    \centering
\end{figure}


The policy evaluation flow can be summarized in two different steps: reduction and combination.
Reduction determines whether a usage policy was written by an authorized personnel and whether the evaluation outcome can be considered or not.
This is achieved by creating an evaluation tree having as root node a root-of-trust policy and as edges the results of applicable policies, then finding the branches that can reach the root-of-trust.
In the combination step, the \ac{pdp} combines all valid results using combining algorithms defined in the policies.
This evaluation flow is described in Listing \ref{lst:evaluationReduction}. 
The \ac{adp} defines a maximum depth of delegations in order to limit number of administrative policies to be evaluated.
Accordingly, if the number of nodes of a path in the reduction graph exceeds the maximum delegation depth, the path will be discarded.
The maximum delegation depth is not supported in this version of our framework.
However, it will be added in future iterations.



\begin{lstlisting}[float=!htb, caption={Example of administrative policies},label={lst:admin_policy}]
policyset l3_ad_policy_set {
  apply denyUnlessPermit
  policy ciso_retain {
    target clause
      regex(Attributes.delegated.action, "\/action\/(.*)") &&
      regex(Attributes.delegate.subject, "\/dataSubjects\/(.*)")
    apply denyUnlessPermit
    rule r1 {
      permit
    }
  }
  policy dataSubject_retain {
    policyIssuer { Attributes.subject = "/dataSubjects/Carol" }
    apply denyUnlessPermit
    target clause
      regex(Attributes.delegated.action, "\/action\/datalifecycle\/(.*)")
        &&
      regex(Attributes.delegate.subject, "\/employees\/manager\/(.*)")
    rule r2 {
      permit
    }
  }
  policy staff_retain {
    policyIssuer {Attributes.subject = "/employees/manager/Joe"}
    apply denyUnlessPermit
    target clause
      Attributes.subject == "/employees/staff/Bob" &&
      Attributes.action == "/action/datalifecycle/retain"
      ...
  }
}
\end{lstlisting}

For both administrative and usage policies, we use the \emph{denyUnlessPermit} combining algorithm due to its deterministic and restrictive nature.
This algorithm is restrictive because its default outcome is always \emph{deny}, unless there is an explicit applicable permit rule.
This also eliminates all indeterministic outcomes, such as \emph{indeterminate}, in cases where an attribute value is missing or a condition is false.
Therefore, if an administrative policy evaluates into \emph{indeterminate}, the corresponding branch would be discarded from the overall delegation tree.
Nonetheless, we intend to analyse the use of different combining algorithms and their effects and risks on the delegation tree and authorization decisions.


\begin{lstlisting}[float=!htb, caption={Evaluation and Reduction process},label={lst:evaluationReduction}, escapeinside={(*}{*)}]
<given a policy set PS and a request R>
evaluate R against PS
find applicable policies Papp
for each applicable policy Pa in Papp
    if deny
        continue
    if PolicyIssuer is absent 
        then combine
    else 
        [selection step] : select administrative policy Padmin for Pa
            for each Pi in Padmin
                create administrative request Ra using R and Pi
                evaluate Ra against Pi
                if deny
                    no edge
                else
                    add edge
                    if !policyIssuer 
                        potential path is found
                    else 
                        go to selection step with Pi as Pa

combine edge results with PS combining algorithm
\end{lstlisting}



\subsection{Data Protection and Retention Policies}
The above model has enabled a new approach to data privacy. 
This is because CISOs can write administrative policies that state the characteristics of delegated actors, such as data owners, and the aspects on which these actors can write policies. 
% Undoubtedly, administrative policies \textit{must} list only allowed operations in order to protect user privacy.
Furthermore, data owners can state possible data manipulation or anonymization techniques to be used whenever their data are used, adding an extra level of protection to better guarantee user privacy.
Since administrative policies can be stored in remote or separate storage or systems, e.g., individual wallets, data owners have the capability of modifying them as they wish. 
Data owners can eventually further delegate some aspects, if they want to, to data managers.
%Having administrative policies under their control enables data owners to better monitor their data usage and authorizations. 

Data lifecycle policies are, afterall, usage control policies, and as such, their rules can be classified by the \ac{ucsp} phases (i.e. ``\textit{pre}'', ``\textit{ongoing}'' and ``\textit{post}'').
Modeling data lifecycle policies according to \ac{ucsp} phases allows data owners to define usage policies related to different stages of a usage session, giving them full control over the lifecycle of their data, from collection to treatment to destruction (Figure \ref{fig:datalifecycle}).
For instance, \ac{ucsp} phases can be used to control the data lifecycle as follows:
\begin{itemize}
    \item \textit{pre}: The data owner needs to provide consent for new data before it is collected and accessed for the first time. First-time collection and access consent may be recurrent over a time period.
    \item \textit{ongoing}: Once data is collected and first-time accessed; consent defines the conditions of data usage, storage, transfer, and etc.
    \item \textit{post}: Finally, once data is no longer used or access/usage is violated, consent defines the conditions of archival, deletion, destruction, and etc. of the data as well as any associated clean-up tasks. 
\end{itemize}
An example of such policy is shown in Listing \ref{lst:data_policy}.
This makes the system GDPR-compliant and reduces the need to trust and rely on third parties for proper data management and security.
EU regulations, such as \ac{eidas} \cite{eu-910-2014}, go exactly in this direction, that is guaranteeing data owners full control on their data and to, eventually, explicitly and dynamically authorize each and every attempt to use their data \cite{gdpr}. 
By adding an additional layer of administrative delegation policy, one can also take into account regulations and norms stated by EU or other authorities \cite{gdpr}.
This is because it is possible to have a first layer where regulation is defined, another where each user states their own administrative policy and finally usage policies are defined.

\begin{lstlisting}[float=!htb, caption={Example of data lifecycle policy},label={lst:data_policy}]
policy staff_retain {
    policyIssuer {Attributes.subject = "/employees/manager/Joe"}
    target clause
      Attributes.subject == "/employees/staff/Bob" &&
      Attributes.action == "/action/datalifecycle/retain" &&
      Attributes.dataRecord.id == "entryData" &&
      Attributes.dataRecord.owner == "dataSubject" &&
    Attributes.purpose == "userExperienceOptimization"
  apply denyUnlessPermit
  rule pre {
    target clause Attributes.ucs.step.pre
    condition Attributes.dataRecord.userConsent
    permit
    on permit {
      obligation create {
        dataRecord = Attributes.dataRecord.id
      }
    }
  }
  rule ongoing {
    target clause Attributes.ucs.step.ongoing
    condition
      Attributes.dataRecord.userConsent and
      Attributes.dataRecord.encrypted and
      Attributes.dataRecord.expiryDate > Attributes.currentTime
    permit
  }
  rule post {
    target clause Attributes.ucs.step.post
    deny
    on deny {
      obligation delete {
        msg = "Record deleted"
        dataRecord = Attributes.dataRecord.id
      }
      obligation notify {
        dataRecord = Attributes.dataRecord.id
        hasUserConsented = Attributes.dataRecord.userConsent
        isEncrypted = Attributes.dataRecord.encrypted
        isDataExpired =
          (Attributes.dataRecord.expiryDate < Attributes.currentTime)
      }
    }
  }
}
\end{lstlisting}


\subsection{Use Case}
In a typical healthcare scenario, a patient shares with hospital personnel many of their personal and sensitive information: address, income, insurance, diseases, whether they have donated an organ or blood, etc. 
The patient needs to properly protect such information from disclosure or from misusage to protect their own privacy. 
To achieve this, the patient can define an administrative policy that states how their data should be managed by the organisation and how the organisation’s policies will be handled.
Such a profile does not require to define a full-fledged policy, rather the patient can delegate someone else as a doctor or a relative. The beauty of using the aforementioned profile is the high degree of flexibility it provides to people in different conditions. To mitigate abuse, patient is delegated by the hospital itself which is the so-called root of trust. 
In the hospital, for example, a policy states that patients’ data can be shared in a medical équipe.
On the other hand, our policy model allows the patient to state that no sharing policies are allowed on their data.
Therefore even if the hospital’s policy can be permitted, the flow ends up in a NotAdmissible state, thus data sharing will be denied.